all: Main

Main: Main.cpp BinaryTree.h
	g++-4.7 -std=c++11 Main.cpp -g -o Main

clean:
	rm Main
	
run:
	./Main