/*
	Matt Gaikema
	Main.cpp
*/
#include <iostream>
#include <fstream>
#include <string>
#include "BinaryTree.h"

using namespace std;

int main()
{
	const string path = "./test_files/221-A4-test-files/";
	string filename = "3p";
	cout << "Choose a file:" << endl;
	cin >> filename;
	
	matt::BinaryTree<int> tree;	
	
	ifstream infile;
	infile.open(path + filename);
	//infile.open("test.txt");
	if (!infile)
	{
		cout << "No file!" << endl;
		return 1;
	}
	
	int contents;
	
	cout << "Input data: " << endl;
	while (infile)
	{
		string c = "";
		infile >> c;
		if (c=="") break;
		contents = stoi(c);
		cout << contents << endl;
		tree.insert(contents);
	}
	infile.close();
	cout << "Create a binary tree." << endl;
	
	cout << "\nCount: " << tree.count() << endl;
	cout << "Total cost: " << tree.cost() << endl;
	cout << "Average search cost: " << (double)tree.cost()/tree.count() << endl;
	cout << "\n";	
	
	ofstream outfile;
	outfile.open("output.txt");
	
	if (tree.count() <= 16)
	{
		// Print to command prompt
		
		cout << "Preorder print: \n";
		tree.preorder_print(cout);
		
		cout << "\n";
		
		cout << "\nPostorder print: \n";
		tree.postorder_print(cout);
	
		cout << "\n";
	
		cout << "\nInorder print: \n";
		tree.inorder_print(cout);
		
		cout << "\n";

		cout << "\nPrinting level by level." << endl;
		tree.print_levels(cout);
		tree.print_levels(outfile);	

		int remove_me;
		cout << "Choose an element to remove:\n";
		cin >> remove_me;
		tree.remove_node(remove_me);
		tree.inorder_print(cout);
	}
	else
	{
		cout << "Tree information will be written to a file." << endl;
		// Write to file
		
		outfile << "Preorder print: \n";
		tree.preorder_print(outfile);
		
		outfile << "\n";
		
		outfile << "\nPostorder print: \n";
		tree.postorder_print(outfile);
	
		outfile << "\n";
	
		outfile << "\nInorder print: \n";
		tree.inorder_print(outfile);

		int remove_me;
		cout << "Choose an element to remove:\n";
		cin >> remove_me;
		tree.remove_node(remove_me);		
	}
	
	return 0;
}