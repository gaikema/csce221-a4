/*
	Matt Gaikema
	BinaryTree.h
*/
#ifndef BINARYTREE_H
#define BINARYTREE_H

#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <queue>
#include <math.h>

using namespace std;

namespace matt{
template <typename T> class BinaryTree; // class declaration

// Leaf/node class
template <typename T>
struct Node
{
	T key_value;
	Node* left;
	Node* right;
	
	int search_cost = 0;
};

// Tree class
template <typename T>
class BinaryTree
{
public:
	BinaryTree();
	~BinaryTree();
	
	void insert(T key);
	Node<T>* search(T key);
	void destroy_tree();
	
	void preorder_print(ostream& os);
	void postorder_print(ostream& os);
	void inorder_print(ostream& os);
	
	int count();
	int cost() {return total_cost;}
	void print_levels(ostream& os);
	
	Node<T>* remove_node(T key);
	
	Node<T>* const get_root(){return root;}
	
private:
	void destroy_tree(Node<T> *leaf);
	void insert(T key, Node<T> *leaf);
	Node<T>* search(T key, Node<T> *leaf);
	
	void preorder_print(Node<T> *leaf, ostream& os);
	void postorder_print(Node<T> *leaf, ostream& os);
	void inorder_print(Node<T> *leaf, ostream& os);
	
	int count(Node<T> *leaf);
	int total_cost = 0;
	void print_levels(Node<T> *leaf, ostream& os);
	void calculate(Node<T> *leaf);
	void calculate(){calculate(root);}
	
	Node<T>* remove_node(T key, Node<T> *leaf);
	
	Node<T>* root;
	Node<T>* smallest_node(Node<T>* leaf);
};


//////////////////
//	DEFINITIONS
//////////////////


/**
* Default constructor.
* Sets the root to NULL.
*/
template <typename T>
BinaryTree<T>::BinaryTree()
{
	root = NULL;
}

/**
* Destructor.
* Just calls destroy_tree()
*/
template <typename T>
BinaryTree<T>::~BinaryTree()
{
	destroy_tree();
}


// DESTROY

/**
* Calls itself on the child nodes, 
* recursively deleting them before deleting itself.
*
* @param the node below which whose children will be destroyed
*/
template <typename T>
void BinaryTree<T>::destroy_tree(Node<T> *leaf)
{
	if (leaf != NULL)
	{
		destroy_tree(leaf->right);
		destroy_tree(leaf->left);
		delete leaf;
	}
}

/**
* Destroy the tree by recursively destroying
* all of the child nodes.
*/
template <typename T>
void BinaryTree<T>::destroy_tree()
{
	destroy_tree(root);
}


// INSERT

/**
* Private insert
*
* @param A key and a subtree to insert the key into
*/
template <typename T>
void BinaryTree<T>::insert(T key, Node<T>* leaf)
{
	if (key < leaf->key_value)
	{
		if (leaf ->left!=NULL)
			insert(key,leaf->left);
		else
		{
			leaf->left = new Node<T>;
			leaf->left->key_value = key;
			leaf->left->left = NULL;
			leaf->left->right = NULL;
		}
	}
	else if (key >= leaf->key_value)
	{
		if (leaf->right!=NULL)
			insert(key,leaf->right);
		else
		{
			leaf->right = new Node<T>;
			leaf->right->key_value = key;
			leaf->right->left = NULL;
			leaf->right->right = NULL;
		}
	}
}

/**
* Public insert
*
* @param a key to insert
*/
template <typename T>
void BinaryTree<T>::insert(T key)
{	
	if (root!=NULL)
		insert(key,root);
	else
	{
		root = new Node<T>;
		root->key_value = key;
		root->left = NULL;
		root->right = NULL;
	}
	/* 
		Calculate the search cost for each node
		every time a new one is added (bad)
	*/
	calculate();
}


// SEARCH

/**
* Private search
*
* @param a key to search for and a subtree to search
*/
template <typename T>
Node<T>* BinaryTree<T>::search(T key, Node<T> *leaf)
{
	if (leaf!=NULL)
	{
		if (key==leaf->key_value)
			return leaf;
		if (key<leaf->key_value)
			return search(key,leaf->left);
		else 
			return search(key,leaf->right);
	}
	else
		return NULL;
}

/**
* Public search.
* Search the tree for key by 
* recursively searching all the
* child nodes.
*
* @param key the element to search for.
*/
template <typename T>
Node<T>* BinaryTree<T>::search(T key)
{
	return search(key, root);
}


// PREORDER PRINT

/**
* Private method to 
* display the elements of the tree
*
* @param the node whose offspring should be printed,
* and the output stream to display the output to. 
*/
template <typename T>
void BinaryTree<T>::preorder_print(Node<T> *leaf, ostream& os)
{
	if(leaf!= NULL)
	{
		os << leaf->key_value << "[" << leaf->search_cost << "] ";
		preorder_print(leaf->left, os);
		preorder_print(leaf->right, os);
	}
}

/**
* Public preorder_print
*
* @param an output stream to display the output to.
*/
template <typename T>
void BinaryTree<T>::preorder_print(ostream& os)
{
	calculate();
	preorder_print(root, os);
}


// POSTORDER PRINT

/**
* Private method to 
* display the elements of the tree
*
* @param the node whose offspring should be printed,
* and an output stream to display the output to.
*/
template <typename T>
void BinaryTree<T>::postorder_print(Node<T> *leaf, ostream& os)
{
	if (leaf != NULL)
	{
		postorder_print(leaf->left, os);
		postorder_print(leaf->right, os);
		os << leaf->key_value << "[" << leaf->search_cost << "] ";
	}
}

/**
* Public postorder_print
*
* @param an output stream for displaying the output
*/
template <typename T>
void BinaryTree<T>::postorder_print(ostream& os)
{
	calculate();
	postorder_print(root, os);
}


// INORDER PRINT

/**
* Private method to 
* display the elements of the tree
*
* @param the node whose offspring should be printed,
* and an output stream to display the output. 
*/
template <typename T>
void BinaryTree<T>::inorder_print(Node<T> *leaf, ostream& os)
{
	if (leaf != NULL)
	{
		inorder_print(leaf->left, os);
		os << leaf->key_value << "[" << leaf->search_cost << "] ";
		inorder_print(leaf->right, os);
	}
}

/**
* Public inorder_print
*
* @param an output stream for displaying the output
*/
template <typename T>
void BinaryTree<T>::inorder_print(ostream& os)
{
	calculate();
	inorder_print(root, os);
}


// COUNT

/**
* Private count
*
* @param the subtree to count
*/
template <typename T>
int BinaryTree<T>::count(Node<T> *leaf)
{
	if (leaf == NULL)
		return 0;
	else
	{
		int counter = 1;
		counter += count(leaf->right);
		counter += count(leaf->left);
		
		return counter;
	}
}

/**
* Public count.
* Display the total number of nodes.
*/
template <typename T>
int BinaryTree<T>::count()
{
	return count(root);
}


// PRINT LEVELS

/**
* Private print levels.
* Print tree one level per line, 
* as well as the level number (search cost)
* of each element.
*/
template <typename T>
void BinaryTree<T>::print_levels(Node<T> *leaf, ostream& os)
{
	if (leaf == NULL)
		return;
	
	int row = 1;
	//leaf->search_cost = row;
	queue<Node<T>*> current, next;
	current.push(leaf);
	while(!current.empty())
	{
		Node<T>* current_node = current.front();
		current.pop();
		if(current_node != NULL)
		{
			//current_node->search_cost = row;
			//os << current_node->key_value << "[" << current_node->search_cost << "] ";
			os << current_node->key_value << " ";
			if(current_node->left) next.push(current_node->left);
			if(current_node->right) next.push(current_node->right);
		}
		if (current.empty())
		{
			row++;
			os << endl;
			swap(current,next);
			//cout << "Row " << row << ": ";
			//cout << current.size();
			
			if (current.size() < pow(2,row-1) && !current.empty())
			{
				int space = pow(2,row-1) - current.size();
				for (int i = 0; i < space; i++)
					os << "x ";
			}
			
		}
	}
}

/**
* Public print levels.
* Print tree one level per line.
*/
template <typename T>
void BinaryTree<T>::print_levels(ostream& os)
{
	print_levels(root, os);
}

/**
* Calculate the search cost for each node.
*/
template <typename T>
void BinaryTree<T>::calculate(Node<T> *leaf)
{
	if (leaf == NULL)
		return;
	
	int i = 1;
	leaf->search_cost = i;
	total_cost = i;
	
	queue<Node<T>*> current, next;
	current.push(leaf);
	
	while(!current.empty())
	{
		Node<T>* current_node = current.front();
		current.pop();
		if(current_node != NULL)
		{
			current_node->search_cost = i;
			total_cost += i;
			next.push(current_node->left);
			next.push(current_node->right);
		}
		if (current.empty())
		{
			i++;
			swap(current,next);
		}
	}
	total_cost--;
}


/**
* Private remove node
*/
template <typename T>
Node<T>* BinaryTree<T>::remove_node(T key, Node<T> *leaf)
{
	// Empty tree
	if (leaf == NULL)
		return leaf;
	
	// Left subtree
	if (key < leaf->key_value)
		leaf->left = remove_node(key, leaf->left);
	// Right subtree
	else if (key > leaf->key_value)
		leaf->right = remove_node(key,leaf->right);
	// Delete this one
	else
	{
		// Leaf has 1 or 0 kids
		if (leaf->left == NULL)
		{
			Node<T>* js = leaf->right;
			delete leaf;
			return js;
		}
		else if (leaf->right == NULL)
		{
			Node<T>* js = leaf->left;
			delete leaf;
			return js;
		}
		
		Node<T>* js = smallest_node(leaf->right);
		
		leaf->key_value = js->key_value;
		
		leaf->right = remove_node(js->key_value, leaf->right);
	}
	return leaf;
}

/**
* Public remove node
*/
template <typename T>
Node<T>* BinaryTree<T>::remove_node(T key)
{
	return remove_node(key, root);
}

/**
* Return the smallest node in the subtree of leaf
*/
template <typename T>
Node<T>* BinaryTree<T>::smallest_node(Node<T>* leaf)
{
	Node<T>* current = leaf;
	
	while(current->left != NULL)
		current = current->left;
	
	return current;
}
}

#endif