# CSCE 221 Assignment 4
* Section 504.
* Assignment [details.](https://www.dropbox.com/s/rfptlv4nlxkwkez/Assignment%20pdf.pdf?dl=0)
* Due March 22.
* Exam March 31.
* [Course web page.](http://courses.cs.tamu.edu/teresa/csce221/csce221-index.html)

---

# Instructions
To compile, type `make`.
To run, type `make run`, or type `./Main`.
To remove the object files, type `make clean`.

Once the program starts, the command prompt will ask for a file name. 
Enter a file name of the format "xy", where "x" is a number 1-10 and "y" is either "l", "r", or "p".

---
